<?php 
namespace BDAA;

include_once('Base.php');
include_once('Thana.php');
include_once('Upazila.php');

use BDAA\Base as Base;
use BDAA\Thana as Thana;
use BDAA\Upazila as Upazila;
/**
 * This class represents a District - the second child on the whole administrative system
 * 
 * 
 * @package Bangladesh_Administrative_Areas
 * @author Md Abdullah Al Maruf <maruf.sylhet@gmail.com>
 * @version 1.0
 
 * @see Base.php
 * @see Upazila.php
 * @see Thana.php
 */

class District extends Base {
    
    protected $iso_3166_2;
    protected $name;
    protected $name_bn;
    protected $lat;
    protected $lon;
    protected $website;
    protected $city_corporation_thanas = array();
    protected $thanas = array();
    protected $upazilas = array();
    
    /**
    * The setup process of the object
    *
    * @see      Base.php
    * @param    array  $options     The object properties 
    */
    public function __construct($options = null) {
        parent::__construct($options);
    }
    
    /**
    * Get the ISO 3166_2 code as an identifier for this District
    *
    * @return   string
    */
    public function getId() {
        return $this->getIso31662();
    }
        
    /**
    * Get the array of Upazila objects for this District
    *
    * @see      Upazila.php
    * @return   array   The array of \BDAA\Upazila objects
    */
    public function getUpazilas() {
        if (is_array($this->upazilas) && !(end($this->upazilas)) instanceof Upazila) {
        
            foreach($this->upazilas as $upazilaName => $upazilaDetails) {
                $upazilaDetails['name'] = $upazilaName;
                $this->upazilas[ $upazilaName ] = new Upazila($upazilaDetails);
            }
        }
        
        return $this->upazilas;
    }
      
        
    /**
    * Get the array of Thana objects for this District. 
    * All the city corporation Thanas are returned 
    * in addition to all the Upazilas mapped to Thanas.
    * 
    * @see      Thana.php
    * @return   array   The array of \BDAA\Thana objects
    */
    public function getThanas() {
        if (is_array($this->thanas) && !(end($this->thanas)) instanceof Thana) {
        
            $this->thanas = $this->getCityCorporationThanas();
            
            foreach($this->upazilas as $thanaName => $thanaDetails) {
                if ($thanaDetails instanceof Thana) {
                    $this->thanas[ $thanaName ] = $thanaDetails;
                } elseif(is_array($thanaDetails)) {
                    $thanaDetails['name'] = $thanaName;
                    $this->thanas[ $thanaName ] = new Thana($thanaDetails);
                }
            }
        }
        
        return $this->thanas;
    }
      
        
    /**
    * Get the array of Thana objects for the city corporation
    * 
    * @see      Thana.php
    * @return   array   The array of \BDAA\Thana objects
    */
    public function getCityCorporationThanas() {
        if (is_array($this->city_corporation_thanas) && !(end($this->city_corporation_thanas)) instanceof Thana) {
        
            foreach($this->city_corporation_thanas as $thanaName => $thanaDetails) {
                $thanaDetails['name'] = $thanaName;
                $this->city_corporation_thanas[ $thanaName ] = new Thana($thanaDetails);
            }
        }
        
        return $this->city_corporation_thanas;
    }
        
    /**
    * Get the ISO 3166_2 code for this District
    *
    * @return   string 
    */
    public function getIso31662() {
        return $this->iso_3166_2;
    }
        
    /**
    * Get the name for this District in English
    *
    * @return   string
    */
    public function getName() {
        return $this->name;
    }    
    
    /**
    * Get the name for this District in Bangla
    *
    * @return   string
    */
    public function getNameBn() {
        return $this->name_bn;
    }
        
    /**
    * Get the administrative website for this District
    *
    * @return   string
    */
    public function getWebsite() {
        return $this->website;
    }
    
    /**
    * Get the approximate latitude of the district centre 
    *
    * @return   string 
    */
    public function getLat() {
        return $this->lat;
    }
    /**
    * Alias for getLat()
    *
    * @return   string 
    */
    public function getLatitude() {
        return $this->getLat();
    } 

    /**
    * Get the approximate longitude of the district centre 
    *
    * @return   string 
    */
    public function getLon() {
        return $this->lon;
    }
    /**
    * Alias for getLon()
    *
    * @return   string 
    */
    public function getLongitude() {
        return $this->getLon();
    }    
}
