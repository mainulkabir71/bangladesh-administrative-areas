<?php
namespace BDAA;

include_once('Division.php');

use BDAA\Division as Division;
/**
 * A class for handling the administrative areas information for Bangladesh.
 * This includes the Divisions, Districts, Upazilas and in some cases Thanas
 * 
 * 
 * @see Division.php
 * @package Bangladesh_Administrative_Areas
 * @author Md Abdullah Al Maruf <maruf.sylhet@gmail.com>
 * @version 1.0
 *
 * @todo add unions of every Upazilas in next increment
 * @todo add Thanas for every District in future
 */

 
class Areas {
    
    /**
    *   The session namespace for storing the array for divisions
    */
    const SESS_BD_AREAS = 'X_bd_areas';
   
    /**
    *  Preparing the array of division objects and making it available in 
    *  a session variable identified by the constant SESS_BD_AREAS
    */
    public function __construct() {    
        @session_start();
        $_SESSION[ self::SESS_BD_AREAS ] = null;
        if (!$this->_classesExistInSession()) {
            $areas = require_once( realpath( dirname(__FILE__) ) . '/../data.php');
            $classes = null;
            
            foreach($areas as $divisionName => $divisionDetails) {            
                $divisionDetails['name'] = $divisionName;
                $classes[ $divisionName ] = new Division($divisionDetails);                
                $classes[ $divisionName ]->getDistricts();
            }
            
            if (null === $classes) {
                throw new Exception("Sorry, something went wrong.");            
            }
                
            $_SESSION[ self::SESS_BD_AREAS ] = $classes;
        }
    }
    

    /**
    *  Get the District objects for the provided Division
    *  
    *  @param   String  $divisionName   The name of the administrative division
    *  @return  array   An array of \BDAA\District objects
    */   
    public function getDistricts($divisionName) {
        if (null === $divisionName) {
            throw new InvalidArgumentException("You need to provide the division name as argument.");                
        }
        
        $session = $this->_getSession();  
        
        if (!isset($session[ $divisionName ])) {
            throw new InvalidArgumentException("Unknown division name '$divisionName'");                
        }
        
        $division = $session[ $divisionName ];
        return $division->getDistricts();
    }


    /**
    *  Get the division object 
    *  
    *  @param   String  $divisionName   The name of the administrative division
    *  @return  \BDAA\Division
    */
    public function getDivision($divisionName) {
        if (null === $divisionName) {
            throw new InvalidArgumentException("You need to provide the division name as argument.");                
        }
        
        $session = $this->_getSession();  
        
        if (!isset($session[ $divisionName ])) {
            throw new InvalidArgumentException("Unknown division name '$divisionName'");                
        }
        
        return $session[ $divisionName ];
    }
    
    
    /**
    * Get all division objects in an array
    *  
    * @return  array    An array of \BDAA\Divisio objects
    */
    public function getAllDivisions() {
        return $this->_getSession();
    }
    
    
    /**
    * Get all division names only
    *  
    * @return  array    An array of Division names only
    */    
    public function getDivisionNames() {        
        $session = $this->_getSession();
        $divNames = array();
        
        foreach($session as $division) {
            $divNames[] = $division->getName();
        }
        
        return $divNames;
    }
    
    
    /**
    * Get all Division names either for all the Divisions or a specific Division
    *
    * @param    string  $division   specify Division name
    * @return   array   An array of District names for the provided Division name
    */    
    public function getDistrictNames($divisionName) {
        if (null === $divisionName) {
            throw new InvalidArgumentException("You need to provide the division name as argument.");                
        }
        
        $session = $this->_getSession(); 
        
        if (!isset($session[ $divisionName ])) {
            throw new InvalidArgumentException("Unknown division name '$divisionName'");                
        }
        
        $districtNames = array();           
        
        if (null !== $divisionName) {
            foreach ($session[ $divisionName ]->getDistricts() as $district) {
                $districtNames[] = $district->getName();
            }
            
            return $districtNames;
        }
        
        foreach($session as $division) {
            foreach($division->getDistricts() as $district) {
                $districtNames[] = $district->getName();
            }
        }
        
        return $districtNames;
    }
    
    
    
    /*
    *   Check if the session has \BDAA\Division classes
    *
    */
    private function _classesExistInSession() {
        return ( isset($_SESSION[ self::SESS_BD_AREAS ]) 
            && is_array($_SESSION[ self::SESS_BD_AREAS ]) 
            && (end($_SESSION[ self::SESS_BD_AREAS ]) instanceof Division)
        );
    }
    
    
    /*
    *   Get the session if classes are stored in session
    *
    */
    private function _getSession() { 
        if (! $this->_classesExistInSession()) {
            throw new Exception("Session not set.");
        }
        
        return $_SESSION[ self::SESS_BD_AREAS ];
    }
}
